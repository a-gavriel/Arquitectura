# ASM x86 Tutorial

### This will be a series of files following the tutorial from:
```
https://asmtutor.com/
```

To compile:
nasm -f elf64 Helloworld.asm

To link:
ld -s -o Helloworld Helloworld.o

To run:
./Helloworld

--------------------------
For x86 32 bits:

nasm -f elf helloworld-input.asm 
ld -m elf_i386 helloworld-input.o -o helloworld-input 
./helloworld-input