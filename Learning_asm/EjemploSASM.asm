%include "io64.inc"

section .text
    global CMAIN

section .data
 hello:     db 'Hello world!',10    ; 'Hello world!' plus a linefeed character
 helloLen:  equ $-hello             ; Length of the 'Hello world!' string
      

CMAIN:
    ;write your code here
    xor rax, rax
    add rax, 1
    ret