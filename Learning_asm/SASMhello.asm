
%include        'io64.inc'
 

;used for storing digits to print
section .bss
	digitSpace resb 100
	digitSpacePos resb 8

section .data
	text db "Hello, World!",10,0

section .text
	global CMAIN

CMAIN:
	MOV	rax,rsp
	call _printiRAX

	rdtsc
	MOV r8,rax
	mov rdi, 49
	call _isprime
	call _printiRAX
	
	MOV	rax,rsp
	call _printiRAX
	
	;exit
	mov rax, 60
	mov rdi, 0
	syscall




;---  functions  ---
;input: rax as pointer to string
;output: print string at rax

;for prints: rdx, length in rbx

_print:
	;stores values
	push rcx
	push rbx
	push rdx

	push rax
	mov rbx, 0
_printLoop:
	inc rax
	inc rbx
	mov cl, [rax]
	cmp cl, 0
	jne _printLoop

	mov rax, 1
	mov rdi, 1
	pop rsi
	mov rdx, rbx
	syscall

	;restores values
	pop rdx
	pop	rbx
	pop rcx
	ret




;prints the integer number in rax
_printiRAX:
	;stores values
	push rcx
	push rbx
	push rax

	mov rcx, digitSpace
	mov rbx, 10
	mov [rcx], rbx
	inc rcx
	mov [digitSpacePos], rcx

;stacks an endline first
;stacks each digit one by one in digitSpace
_printiRAXLoop:
	mov rdx, 0
	mov rbx, 10
	div rbx
	push rax
	add rdx, 48

	mov rcx, [digitSpacePos]
	mov [rcx], dl
	inc rcx
	mov [digitSpacePos], rcx
	
	pop rax
	cmp rax, 0
	jne _printiRAXLoop

;pops the stack in digitSpace and prints
_printiRAXLoop2:
	mov rcx, [digitSpacePos]

	mov rax, 1
	mov rdi, 1
	mov rsi, rcx
	mov rdx, 1
	syscall

	mov rcx, [digitSpacePos]
	dec rcx
	mov [digitSpacePos], rcx

	cmp rcx, digitSpace
	jge _printiRAXLoop2

	;restores values
	pop rax
	pop rbx
	pop rcx
	ret


;in: rdi
;out:rax
_isprime:
	;saves values
	push rcx
	push rdi
	push rdx
	push r8
	
	mov rdx,0
	cmp rdi,2			;if it's less than 2 
	jl 	_isprimeFalse
	cmp rdi,2			;if it's 2
	je 	_isprimeTrue
	cmp rdi,3			;if it's 3
	je 	_isprimeTrue
	test	dil,1		;checks parity
	jz	_isprimeFalse ;jumps if even	
	mov	rax,rdi
	mov	r8,3
	idiv r8
	cmp	rdx,0			;if remainder of num/3 is 0
	je	_isprimeFalse
	mov	rcx,5			;starts counter

_primeloop:
	mov	rax,rdi
	idiv	rcx
	cmp	rdx,0
	je	_isprimeFalse
	add rcx,2
	mov	rax,rcx
	mul	rax
	cmp	rdi,rax
	jl	_isprimeTrue
	jmp	_primeloop

_isprimeFalse:
	mov rax,0
	jmp _isprimeexit
_isprimeTrue:
	mov rax,1
	jmp _isprimeexit

_isprimeexit:
	pop r8
	pop rdx
	pop rdi
	pop rcx
	ret