
%include        'functions64.asm'
 

;used for storing digits to print
section .bss
	digitSpace resb 100
	digitSpacePos resb 8
	primep		resq 1
	primeq		resq 1
	keyn		resq 2
	phin		resq 2
	keye		resq 1


section .data
	text db "Hello, World!",10,0
	

section .text
	global _start

_start:
	mov r12,128
	mov	rax,rsp
	call _printiRAX

	call _getprime
	call _printiRAX
	mov   rdi, rax
	call _isprime
	call _printiRAX
	
	mov	rax,rsp
	call _printiRAX
	
	;exit
	mov rax, 60
	mov rdi, 0
	syscall



