global data1, data2

def lectura_archivo1():
    global data1
    line = ""
    data1 = ""
    #print("filename1: ")
    filename = "Helloworld64.asm"
    with open(filename) as f:
        lines = f.readlines()
    for line in lines:        
        line = line.replace("functions64.asm", "io64.inc")        
        line = line.replace("_start", "CMAIN")

        #print(line)
        data1+=line
    #print(data1)
    f.close()


def lectura_archivo2():
    global data2    
    data2 = ""
    #print("functions filename: ")
    filename = "functions64.asm"
    with open(filename) as f:
        data2 = f.read()
    #print (data2)
    f.close()


def genSASM():
    lectura_archivo1()
    lectura_archivo2()



    #print("outputname: ")
    filename = "SASMhello.asm"
    f = open(filename, "r+")
    f.seek(0)
    f.truncate()

    finaldata = data1 +"\n;---  functions  ---\n"+ data2
    f.write(finaldata)


genSASM()
