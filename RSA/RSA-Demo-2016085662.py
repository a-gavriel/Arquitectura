######################
#  Instituto Tecnológico de Costa Rica
#  Alexis Gavriel Gómez
#  2016085662
#  Arquitectura de Computadores 1
######################

'''
RSA algorithm for encryption and decryption of data
User A generates and shares 2 public keys, n and e
User A generates private key: d
User B takes a message and encrypts it with them and sends the new message to A
User A decrypts the message using d


generating 2 different prime numbers: p, q
calculates n = p * q
calculates phi(n), which equals the amount of coprimes of p and q under n
using Euler's formula: (p-1)(q-1)
chooses e, the public encryption key, e must be coprime with n and phi(n) and less than phi_n
  iterates through numbers until finding the smallest e
finds the smallest d so that (k*phi_n + 1)/e is an integer
converts the message into an integer value
Data encryption formula:
(message ** e ) % n
Data decryption formula:
(message ** d ) % n

'''




from random import randint
from math import gcd as bltin_gcd

def is_prime(n):
  if n < 2:
    return False
  elif(n == 2) or (n == 3):
    return n
  elif (n%2 == 0) or (n%3 == 0):
    return False
  else:
    i = 5
    while (i * i) < n:
      if (n%i == 0) or (n%(i+2) == 0):
        return False
      else:
        i += 6
    return True

# Generating random prime numbers
def gen_prime():  
  random = randint(5,15)
  if random % 2 == 0:
    random += 1
  while(not is_prime(random)):
    random += 2
  return random

# using python built in gcd
def coprime(a, b):
    return bltin_gcd(a, b) == 1

def de_en_cryption(de , n, message):
  msgexp =  (message ** de )
  return msgexp % n


def RSA(msg):

  
  # generating 2 different prime numbers: p, q
  p = gen_prime()
  q = p
  while (q == p):
    q = gen_prime()

  p = 11
  q = 5
  #print("primes are: " + str(p) + " , " + str(q))

  # calculates n
  n = p * q

  #print("n =" , n)

  # calculates phi(n), which equals the amount of coprimes of p and q under n
  # using Euler's formula: (p-1)(q-1)
  phi_n = (p-1) * (q-1)

  # chooses e, the public encryption key, e must be coprime with n and phi(n) and less than phi_n
  e = 2
  #print("phi_n = " + str(phi_n))
  while ( not ((coprime(n,e)) and (coprime(phi_n,e))) and e < phi_n ):
    e += 1

  d = 1
  k = 1
  
  # finds the smallest d so that (k*phi_n + 1)/e is an integer
  while ( (k*phi_n + 1)%e != 0 ):
    d = (k*phi_n + 1)/e
    k += 1
  d = (k*phi_n + 1)/e
 # print("k",k)
  d = int (d)

  

  # message to be encrypted
  message = msg
  
  # Data encryption:
  # (message ** e ) % n
  encrypt = de_en_cryption( e, n, message)

  print ("msg",msg)
  
  temp1 = encrypt**d
  if (temp1 - 2**128)>0:
    print ("\t\t not worked")
    #print("encrypt = " + str(encrypt))
  else:
    #print("\t",temp1)
    #print("encrypt = " + str(encrypt))
    None

  # Data decryption:
  # (message ** d ) % n  
  
  decrypt = de_en_cryption( d, n, encrypt)
  print("decrypt = " + str(decrypt))

  return (temp1 - 2**128)<0

def getprimes():
  last = 1000
  prime_list = []
  for i in range(last):
    if (is_prime(i)):
      print(i)
      prime_list.append(i)
  
print("d = ",37)
count = 0
for a in range(60):
  x = int( RSA(a) )
  count+=x
  print("----")
print (count)
#getprimes()
#RSA()
