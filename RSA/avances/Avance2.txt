


Tamaño de llaves:
Debido a que el producto de dos números de X bytes produce un valor de tamaño 2*X bytes, 
	se define que se eligirán números primos de el tamaño de la arquitecura. Esto para que la primera llave pública generada sea de un tamaño equivalente al doble de la arquiectura. 

Se debe considerar que el tamaño de salida de los datos tiene que ser equivalente al tamaño de los datos de entrada. 
RSA solo puede encriptar datos hasta un tamaño equivalente al tamaño de la llave. Debido a esto los mensajes a encriptar será de 64 bits. Se utilizará un parsing para convertir el archivo de texto original en números utilizando el formato de la base64, esta contiene números del 0-9 y letras A-Z y a-z. Si el documento fuera más grande que este tamaño permitido de 64 bits, se puede partir dicho documento y aplicar el mismo RSA en todas las partes. 

https://es.wikipedia.org/wiki/Base64
https://tls.mbed.org/kb/cryptography/rsa-encryption-maximum-data-size

Autenticación de dato ecriptado:
https://www.mobilefish.com/services/rsa_key_generation/rsa_key_generation.php
