%include        'io64.inc'

SECTION .data
msg     db      '15 and 13 gcd: ', 0Ah ; we can modify this now without having to update anywhere else in the program
 
section .text
	global CMAIN

CMAIN:
    mov rbp, rsp; for correct debugging
    mov r8,0
    mov r9,0
    add     r8, 35
    add     r9, 15
    mov 	r10,r8
    mov 	r11,r9
gcd:
	cmp		r10, r11
	je 		end
	jl 		less
	sub 	r10 , r11
	jmp 	gcd
less:
	sub 	r11, r10
	jmp 	gcd
end:
        mov rax,r10


	;exit
	mov rax, 60
	mov rdi, 0
	syscall

