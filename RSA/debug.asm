
%include        'functions64.asm'
 

;used for storing digits to print
section .bss
	digitSpace resb 100
	digitSpacePos resb 8
	ptrs 		resq 10
	primep		resq 1
	primeq		resq 1
	keyn		resq 2
	phin		resq 2
	keye		resq 2
	keyd		resq 2


section .data
	text db "-stack pointer: ",0
	t_time db "-tiempo: ",0
	t_mod db "-stack pointer: ",0
	exam1 dq  7,-2
	exam2 dq  1,3
	exam3 dq  0,50000
	exam4 dq  0,5
	exam5 dq  0,547
	temp1 times 2 dq 0
	temp2 times 2 dq 0
	temp22 times 2 dq 0
	temp23 times 2 dq 0
	temp24 times 2 dq 0
	temp25 times 2 dq 0
	temp26 times 2 dq 0
	temp3 times 4 dq 0
	temp4 times 4 dq 0
	

section .text
	global _start

_start:
	;prints the stack pointer
	mov rax,text
	call _print
	call _printstack
	;--------------------

	rdtsc
	mov r9,temp26
    mov [r9],rdx
	add r9,8
	mov [r9],rax


	mov rcx,128
	mov rdi, exam1
	mov rsi,exam2
	mov r8,temp1
	mov r11,temp2
	mov rax,exam3

    

	call _mult128
	call _modr128
	call _sub
	call _shiftl

	rdtsc
	mov r10,temp25
    mov [r10],rdx
	add r10,8
	mov [r10],rax


	mov rax,t_mod
	call _print
	mov r8,exam3
	call _printregs


	mov rdi,temp25
	mov rsi, temp26
	mov rax,temp24

	call _sub

	mov rax,t_time
	call _print
	mov r8,temp24
	call _printregs

	mov rdi,exam3
	mov rsi,exam4
	mov rbx,exam5
	mov rax,temp22
	mov rcx,128
	mov r10,temp1
	mov r11,temp2
	
	;call _modularexp
	mov r8,temp22
	;call _printregs
	


	mov rdx,2
	mov rsi,exam2
	mov rbx,exam1
	mov rcx,exam3
	mov rdi,temp1
	mov r10,temp2




	;prints the stack pointer
	mov rax,text
	call _print
	call _printstack
	;--------------------

	;exit program
	mov rax, 60
	mov rdi, 0
	syscall
	;--------------------
