# RSA
## Autor: Alexis Gavriel Gómez
### Carné: 2016085662

Sistema:
* OS: Linux x64

Programas requeridos:

* Nasm (para x84_64)

* SASM (para debugger)

Para compilar, linkear y ejecutar:
```
nasm -f elf64 RSA.asm
ld -s -o RSA RSA.o
./RSA
```


No se realizó por completo el algoritmo de encriptación. Se logró realizar las operaciones de:
* Suma/Resta genéricas
* Operaciones de desplazamiento genéricas
* Multiplicación para 128 bits
* Operación módulo para 128 bits

No se logró realizar el algoritmo de exponenciación modular para encriptar o desencriptar los datos.

Para probar dichas operaciones se puede utilizar el archivo "debug.asm"

```
nasm -f elf64 debug.asm
ld -s -o debug debug.o
./debug
```

El archivo genSASM.py junta los archivos RSA.asm y functions64.asm en un único archivo y cambia los headers para poder ejecutarse en SASM.

### Con respecto a las métricas

Se puede estimar el tiempo tomado por una función utilizando la funcion "rdtsc".
Al tomar el tiempo antes y después de que se ejecute una función, y luego restando los tiempo se obtiene una aproximación de la duración en tiempo del reloj.

En las líneas 64-67 se puede comentar varias funciones para calcular el tiempo tomado por una, o un conjunto de varias.