
%include        'functions64.asm'
 

;used for storing digits to print
section .bss
	digitSpace resb 100
	digitSpacePos resb 8
	ptrs 		resq 10
	primep		resq 1
	primeq		resq 1
	keyn		resq 2
	phin		resq 2
	keye		resq 1
	keyd		resq 2


section .data
	text db "-stack pointer: ",0
	t_n db "-n: ",0
	t_phi_n db "-phi(n): ",0
	t_text db "-e: ",0
	exam1 dq  18446744073709551615,18446744073709551615
	exam2 dq  1,1
	exam3 dq  000010002,300040005
	temp1 times 2 dq 0
	temp2 times 2 dq 0
	temp3 times 2 dq 0
	temp4 times 4 dq 0
	temp5 times 4 dq 0
	

section .text
	global _start

_start:
	;gets pq in r9 and rax
	mov rcx,128
	call _pq_n_phi

	mov rax,t_n
	call _print
	mov r8,keyn
	call _printregs

	mov rax,t_phi_n
	call _print
	mov r8,phin
	call _printregs

	call _str_e



	;exit program
	mov rax, 60
	mov rdi, 0
	syscall
	;--------------------



;store e 65537
_str_e:
	push r9
	push r10
	mov r9,keye
	mov r10,65537
	mov [r9],r10
	pop r10
	pop r9
	ret

;--- RSA functions
_pq_n_phi:
;gens first prime   r9
	xor rax,rax
	call _getprime
	mov r9,rax
;gens second prime  r10
	mov rax,1
	call _getprime
	mov r10,rax
;if equal, make another 2 primes
	cmp r9,r10
	je _pq_n_phi
;else, saves them
	mov r8,primep
	mov [r8],r9
	mov r8,primeq
	mov [r8],r10
	

	; generates key n = p*q, and saves
	mov rdx,0
	mul r9
	mov r8,keyn
	mov [r8],rdx
	add r8,8
	mov [r8],rax

	; generates phi_n = (p-1)*(q-1) and saves
	sub r9,1
	sub r10,1
	mov rax,r9
	mov rdx,0
	mul r10

	mov r8,phin
	mov [r8],rdx
	add r8,8
	mov [r8],rax

	ret
