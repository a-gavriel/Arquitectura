
%include        'io64.inc'
 

;used for storing digits to print
section .bss
	digitSpace resb 100
	digitSpacePos resb 8
	ptrs 		resq 10
	primep		resq 1
	primeq		resq 1
	keyn		resq 2
	phin		resq 2
	keye		resq 1
	keyd		resq 2


section .data
	text db "-stack pointer: ",0
	exam1 dq  18446744073709551615,18446744073709551615
	exam2 dq  1,1
	exam3 dq  000010002,300040005
	temp1 times 2 dq 0
	temp2 times 2 dq 0
	temp3 times 2 dq 0
	temp4 times 4 dq 0
	temp5 times 4 dq 0
	

section .text
	global CMAIN

CMAIN:
	;gets pq in r9 and rax
	call _pq_n_phi
	call _str_e



	;exit program
	mov rax, 60
	mov rdi, 0
	syscall
	;--------------------



;store e 65537
_str_e:
	push r9
	push r10
	mov r9,keye
	mov r10,65537
	mov [r9],r10
	pop r10
	pop r9
	ret

;--- RSA functions
_pq_n_phi:
;gens first prime   r9
	xor rax,rax
	call _getprime
	mov r9,rax
;gens second prime  r10
	mov rax,1
	call _getprime
	mov r10,rax
;if equal, make another 2 primes
	cmp r9,r10
	je _pq_n_phi
;else, saves them
	mov r8,primep
	mov [r8],r9
	mov r8,primeq
	mov [r8],r10
	

	; generates key n = p*q, and saves
	mov rdx,0
	mul r9
	mov r8,keyn
	mov [r8],rdx
	add r8,8
	mov [r8],rax

	; generates phi_n = (p-1)*(q-1) and saves
	sub r9,1
	sub r10,1
	mov rax,r9
	mov rdx,0
	mul r10

	mov r8,phin
	mov [r8],rdx
	add r8,8
	mov [r8],rax

	ret

;---  functions  ---
;input: rax as pointer to string
;output: print string at rax
;for prints: rdx, length in rbx

_print:
	;stores values
	push rcx
	push rbx
	push rdx
	push rax
	mov rbx, 0
_printLoop:
	inc rax
	inc rbx
	mov cl, [rax]
	cmp cl, 0
	jne _printLoop

	mov rax, 1
	mov rdi, 1
	pop rsi
	mov rdx, rbx
	syscall

	;restores values
	pop rdx
	pop	rbx
	pop rcx
	ret

;-------------------------------------------------------------------------
;prints the integer number in rax in base 10
_printiRAX:
	;stores values
	push rcx
	push rbx
	push rax
	push rdx
	push rdi 
	push rsi
	push r11

	mov rcx, digitSpace
	mov rbx, 10
	mov [rcx], rbx
	inc rcx
	mov [digitSpacePos], rcx

;stacks an endline first
;stacks each digit one by one in digitSpace
_printiRAXLoop:
	mov rdx, 0
	mov rbx, 10
	div rbx
	push rax
	add rdx, 48

	mov rcx, [digitSpacePos]
	mov [rcx], dl
	inc rcx
	mov [digitSpacePos], rcx
	
	pop rax
	cmp rax, 0
	jne _printiRAXLoop

;pops the stack in digitSpace and prints
_printiRAXLoop2:
	mov rcx, [digitSpacePos]

	mov rax, 1
	mov rdi, 1
	mov rsi, rcx
	mov rdx, 1
	syscall

	mov rcx, [digitSpacePos]
	dec rcx
	mov [digitSpacePos], rcx
	cmp rcx, digitSpace
	jge _printiRAXLoop2

	;restores values
	pop r11
	pop rsi
	pop rdi
	pop rdx
	pop rax
	pop rbx
	pop rcx
	ret

; prints *r8, rcx = size
_printregs:
	push rdx
	push rax
	push rcx
	call _lenrcx
	mov rdx,r8
_printregsl:
	mov  rax,[rdx]
	call _printiRAX
	cmp rcx,0
	je _printregsexit
	add rdx,8
	sub rcx,8
	jmp _printregsl
_printregsexit:
	pop rcx
	pop rax
	pop rdx
	ret








;-------------------------------------------------------------------------
;in: rdi
;out:rax
;verifies if number is prime
_isprime:
	;saves values
	push rcx
	push rdi
	push rdx
	push r8
	
	mov rdx,0
	cmp rdi,2			;if it's less than 2 
	jl 	_isprimeFalse
	cmp rdi,2			;if it's 2
	je 	_isprimeTrue
	cmp rdi,3			;if it's 3
	je 	_isprimeTrue
	test	dil,1		;checks parity
	jz	_isprimeFalse ;jumps if even	
	mov	rax,rdi
	mov	r8,3
	idiv r8
	cmp	rdx,0			;if remainder of num/3 is 0
	je	_isprimeFalse
	mov	rcx,5			;starts counter

_isprimeloop:
	mov	rax,rdi
	idiv	rcx
	cmp	rdx,0
	je	_isprimeFalse
	add rcx,2
	mov	rax,rcx
	mul	rax
	cmp	rdi,rax
	jl	_isprimeTrue
	jmp	_isprimeloop

_isprimeFalse:
	mov rax,0
	jmp _isprimeexit
_isprimeTrue:
	mov rax,1
	jmp _isprimeexit
_isprimeexit:
	pop r8
	pop rdx
	pop rdi
	pop rcx
	ret

;-------------------------------------------------------------------------
;gets a prime number in rax
_getprime:	
	push rdi
	push rdx
	push r8

	mov r8,1
	shl r8,35

	add rax,0
	jnz  _getprimel
_getprimes:
	rdtsc
	or rax,r8
	shr rax,20
	jmp _getprimec
_getprimel:
	rdtsc
	or rax,r8
	shr rax,20
_getprimec:
	mov rdi,rax
	test dil,1
	jnz	 _getprimeloop ;jumps if odd
	inc rdi
_getprimeloop:
	call _isprime
	cmp	rax,1
	je _getprimefound
	add rdi,2
	jmp _getprimeloop
_getprimefound:
	mov rax,rdi

	pop r8
	pop rdx
	pop rdi
	ret

;-------------------------------------------------------------------------
;prints the stack pointer as integer b10
_printstack:
	push rax
	mov	rax,rsp
	call _printiRAX
	pop rax
	ret

;-------------------------------------------------------------------------
;calculates the amount of registers to move to get the LSB
;gets the amount of bytes in rcx
;in: size of data, ex: 64, 128, 256
;rcx/8 - 8
;64 => 0
;128=> 8
;256=> 16
_lenrcx:
	push rax
	push r8
	push rdx
	
	mov rdx ,0
	mov rax,rcx ;places size in rax
	mov r8,8
	idiv r8		;calculates amount of steps
	sub rax,8	;calculates final register
	mov rcx,rax ;places final counter in rcx	

	pop rdx
	pop r8
	pop rax
	ret


;-------------------------------------------------------------------------
;in *rax , rcx=size
; clears the data in *rax
_cleardata:
	push rax
	push rcx
	push r8

	mov r8,0
	call _lenrcx
	add rcx,8
	jmp _clearloop

_cleardatax2:
	push rax
	push rcx
	push r8

	add rcx,rcx
	mov r8,0
	call _lenrcx
	add rcx,8
	jmp _clearloop

_clearloop:
	mov [rax],r8
	add rax,8
	sub rcx,8	
	jnz _clearloop

	pop r8
	pop rcx
	pop rax
	ret


;----------------------------------------------------------------------------------


; 			addition / subtraction


;----------------------------------------------------------------------------------



;-------------------------------------------------------------------------
;in: *rdi , *rsi , *rax, rcx = size of numbers in bits
;out: *rax
; generic addition of values 
_add:
	push rdi
	push rsi
	push rax
	push rcx
	push rdx
	push r8
	push r9
	push r10

	;gets the position displacement in rcx of the last register of data
	call _lenrcx

	mov r10,0
	mov rdx,0
	mov r8,0
	mov r9,0
	add rdi,rcx
	add rsi,rcx
	add rax,rcx

	mov r8,[rdi]
	mov r9,[rsi]

	add r8,r9
	setb dl
	mov [rax],r8

	cmp rcx,0
	je _addexit
		
_addloop:
	sub rax,8
	sub rdi,8
	sub rsi,8
	mov r8,[rdi]
	mov r9,[rsi]


	add r8,rdx
	setb r10b
	add r8,r9
	setb dl
	or  rdx,r10
	mov [rax],r8

	sub rcx,8
	cmp rcx,0
	je _addexit
	jmp _addloop

_addexit:
	pop r10
	pop r9
	pop r8
	pop rdx
	pop rcx
	pop rax
	pop rsi
	pop rdi	
	ret

;-------------------------------------------------------------------------
;in: *rdi , *rsi , *rax, rcx = size of numbers in bits
;out: *rax
; generic subtraction
_sub:
	push rdi
	push rsi
	push rax
	push rcx
	push rdx
	push r8
	push r9
	push r10

	;gets the position displacement in rcx of the last register of data
	call _lenrcx

	mov r10,0
	mov rdx,1
	add rdi,rcx
	add rsi,rcx
	add rax,rcx

	mov r8,[rdi]
	mov r9,[rsi]
	not r9

	add r8,rdx
	setb r10b
	add r8,r9
	setb dl
	or rdx,r10
	mov [rax],r8

	cmp rcx,0
	je _subexit
		
_subloop:
	sub rax,8
	sub rdi,8
	sub rsi,8
	mov r8,[rdi]
	mov r9,[rsi]
	not r9

	add r8,rdx
	setb r10b
	add r8,r9
	setb dl
	or rdx,r10
	mov [rax],r8

	sub rcx,8
	cmp rcx,0
	je _subexit
	jmp _subloop

_subexit:
	pop r10
	pop r9
	pop r8
	pop rdx
	pop rcx
	pop rax
	pop rsi
	pop rdi	
	ret

;----------------------------------------------------------------------------------


; 			multiplications


;----------------------------------------------------------------------------------

;in: *rsi, *rdi, rcx = size
;in: *rax, *r8
;out *rax, rdx = overflow
; *rax is the double of length from parameters
; *r8 is temporal register, same size as rax
;
; ptrs store:   0 rcx = size , 
;				8  total position displacement, 16  total position displacementx2,
;				24  ptr 1st value, 32  ptr 2nd value, 40  ptr temp value, 48  ptr result value
;				56 ptr end of 1st,  64  ptr actual 2nd, 72  ptr actual temp

_mult128:
	push rdi
	push rsi
	push rax
	push rcx
    push r8
	push r9
	push r10
	push r11

	;ptr to important values
	mov r10,ptrs
	add r10,24
	mov [r10],rdi
	add r10,8
	mov [r10],rsi
	add r10,8
	mov [r10],r8
	add r10,8
	mov [r10],rax


    ;shifts the pointer to each value to their last register
	add rdi,8
	add rsi,8
	add rax,24
	add r8,24
	

	;PTR56   saves end of  rdi first value
	add r10,8
	mov [r10],rdi

	;PTR64   saves current rsi second value
	add r10,8
	mov [r10],rsi

	;PTR72   saves current temp
	add r10,8
	mov [r10],r8

	;--- begin mult
	mov r8,ptrs
	add r8,72
	mov r11,[r8]	;temp pointer end
	mov rdx,0	;resets rdx to 0
	mov rax,0	;resets rax to 0

	mov rax,[rdi] 	;*rax = first number
	mov r8,[rsi]	;*r8 = second number
	mov r9,0		;stores overflow for each mult

	mul r8
	mov r9,rdx
	mov [r11],rax

	sub rdi,8
	sub r11,8

	mov rdx,0	;resets rdx to 0
	mov rax,0	;resets rax to 0
	mov rax,[rdi] 	;*rax = first number
	
	mul r8
	add rax,r9
	mov [r11],rax

	sub r11,8
	mov [r11],rdx

	mov rcx,256
	call _multadd

	sub rsi,8
	add rdi,8		;restores to end
	
	mov r8,ptrs
	add r8,72
	mov r11,[r8]	;temp pointer end
	sub r11,8		;temp pointer end shift
	mov rdx,0	;resets rdx to 0
	mov rax,0	;resets rax to 0
	
	mov rax,[rdi] 	;*rax = first number
	mov r8,[rsi]	;*r8 = second number
	mov r9,0		;stores overflow for each mult

	mul r8
	mov r9,rdx
	mov [r11],rax

	sub rdi,8
	sub r11,8

	mov rdx,0	;resets rdx to 0
	mov rax,0	;resets rax to 0
	mov rax,[rdi] 	;*rax = first number
	
	mul r8
	add rax,r9
	mov [r11],rax

	sub r11,8		;stores last overflow too
	mov [r11],rdx

	mov rcx,256
	call _multadd

	;restores all values
	pop r11
	pop r10
	pop r9
    pop r8
	pop rcx
	pop rax
	pop rsi
	pop rdi

	ret

;---------------------------
_mult256:
	push rdi
	push rsi
	push rax
	push rcx
    push r8
	push r9
	push r10
	push r11

	;ptr to important values
	mov r10,ptrs
	add r10,24
	mov [r10],rdi
	add r10,8
	mov [r10],rsi
	add r10,8
	mov [r10],r8
	add r10,8
	mov [r10],rax


    ;shifts the pointer to each value to their last register
	add rdi,24
	add rsi,24
	add rax,56
	add r8,56
	

	;PTR56   saves end of  rdi first value
	add r10,8
	mov [r10],rdi

	;PTR64   saves end of rsi second value
	add r10,8
	mov [r10],rsi

	;PTR72   saves end of temp
	add r10,8
	mov [r10],r8

	;--- begin mult
	mov r8,ptrs
	add r8,72
	mov r11,[r8]	;temp pointer end
	

	mov rax,[rdi] 	;*rax = first number
	mov r8,[rsi]	;*r8 = second number
_mult256l1:
	

	mov rdx,0	;resets rdx to 0
	mov rax,0	;resets rax to 0
	mov r9,0		;stores overflow for each mult
	
	mul r8
	mov r9,rdx
	mov [r11],rax

	sub rdi,8
	sub r11,8

	mov rdx,0	;resets rdx to 0
	mov rax,0	;resets rax to 0
	mov rax,[rdi] 	;*rax = first number
	
	mul r8
	add rax,r9
	mov [r11],rax

	sub r11,8
	mov [r11],rdx

	mov rdx,ptrs
	add rdx,24
	mov rax,[rdx] ; gets ptr rdi start
	cmp rax,rdi
;	je _mult256l1exit	;if it was the last register exit
	sub rdi,8
	sub r11,8
	jmp _mult256l1		;else iterates rdi

	mov rcx,256
	call _multadd

	sub rsi,8
	add rdi,8		;restores to end
	
	mov r8,ptrs
	add r8,72
	mov r11,[r8]	;temp pointer end
	sub r11,8		;temp pointer end shift
	mov rdx,0	;resets rdx to 0
	mov rax,0	;resets rax to 0
	
	mov rax,[rdi] 	;*rax = first number
	mov r8,[rsi]	;*r8 = second number
	mov r9,0		;stores overflow for each mult

	mul r8
	mov r9,rdx
	mov [r11],rax

	sub rdi,8
	sub r11,8

	mov rdx,0	;resets rdx to 0
	mov rax,0	;resets rax to 0
	mov rax,[rdi] 	;*rax = first number
	
	mul r8
	add rax,r9
	mov [r11],rax

	sub r11,8		;stores last overflow too
	mov [r11],rdx

	mov rcx,256
	call _multadd

	;restores all values
	pop r11
	pop r10
	pop r9
    pop r8
	pop rcx
	pop rax
	pop rsi
	pop rdi

	ret

;---------------------------
; necesary adder for the multiplication
_multadd:
	;------------ add temp and result
	;---- then clears temp
	;needs rcx
	push rax
	push rdi
	push rsi
	push r8

	mov r8,ptrs	
	add r8,40	
	mov rdi,[r8]		; loads temp*
	add r8,8	
	mov rsi,[r8]		; loads result*	
	mov rax,[r8]		;loads result*
	call _add
	mov rax,rdi
	call _cleardata

	pop r8
	pop rsi
	pop rdi
	pop rax

	;------------ add
	ret

;----------------------------------------------------------------------------------


; 			shifts


;----------------------------------------------------------------------------------
; shift left
; rdi*, rax* , rcx
_shiftl:
    push r8
    push r9
    push r10
	push rcx
	push rdi
	push rax
    
    xor r8,r8
    xor r10,r10

    call _lenrcx
    add rdi,rcx     ;goes to the end
    add rax,rcx     ;goes to the end
    
_shiftl_loop:

    mov r9,[rdi]
    shl r9,1
    setb r8b
    add r9,r10
	mov [rax],r9
    mov r10,r8
    

    add rcx,0
    jz _shiftl_end
    sub rdi,8
	sub rax,8
    sub rcx,8
    jmp _shiftl_loop

_shiftl_end:
	pop rax
	pop rdi
	pop rcx
    pop r10
    pop r9
    pop r8

    ret
; ---------------------------------------
; shift right
; rdi*, rax* , rcx
_shiftr:
    push r8
    push r9
    push r10
	push rcx
	push rdi
	push rax
    
    xor r8,r8
    xor r10,r10

    call _lenrcx
    
_shiftr_loop:

    mov r9,[rdi]
    shr r9,1
    setb r8b
    add r9,r10
	mov [rax],r9
    shl r8,63
    mov r10,r8
    xor r8,r8

    add rcx,0
    jz _shiftr_end
    add rdi,8
	add rax,8
    sub rcx,8
    jmp _shiftr_loop

_shiftr_end:
	pop rax
	pop rdi
	pop rcx
    pop r10
    pop r9
    pop r8

    ret




;----------------------------------------------------------------------------------
; cmp function 
;in rsi*, rdi*, r8* temp same size, rcx = size
;out rax  =  negative | notZero ;  0 =>  0 | 0  ; 3 => 0 | 1 ; -2: 1 | 1
_cmp:
	push r9
	push r10
	push rcx
	push rdi
	push rsi
	push r8

	xor r10,r10
	mov rax,r8
	call _sub
	call _lenrcx
	mov r9,[r8]
	add r9,0
	jl _cmpless
	mov rax,0
	jmp _cmploop
_cmpless:
	mov rax,1
_cmploop:
	mov r9,[r8]
	add r9,0
	jnz _cmpnz
	cmp rcx,0
	je  _cmpexit
	sub rcx,8
	add r8,8
	jmp _cmploop
_cmpnz:
	mov r10,1
_cmpexit:
	shl rax,1
	add rax,r10

	pop r8
	pop rsi
	pop rdi
	pop rcx
	pop r10
	pop r9
	ret


;----------------------------------------------------------------------------------
; calcs the module: rdi % rsi
;in rdi*, rsi*, sub temp r8*  X = r11* temp same size
;out rax*
_modr128:
	push r9
	push r10
	push rax
	

	;copy *rdi to *rax to save
	push rbx
	mov r10,rdi
	mov rbx,rax
	mov r9,[r10]
	mov [rbx],r9
	add r10,8
	add rbx,8
	mov r9,[r10]
	mov [rbx],r9
	pop rbx
	
	


	mov r9,0
	mov r10,0
	mov rcx,128
	mov rdx,rax
	mov rax,r11



	call _sub
	mov  rax,[r11]		; if less than
	add  rax,0
	jl  _modr128_l		; trying to get 4 % 5, return 4
	mov rax,r11
	call _cleardata

	; moves value:  X = B ; x temp
	mov r9,[rsi]
	mov [r11],r9
	
	add rsi,8
	add r11,8
	mov r9,[rsi]
	mov [r11],r9
	
	sub rsi,8
	sub r11,8
	
	mov rax,r8
	call _cleardata
	;saves last bit of A in r10
	add rdi,8
	mov r9,[rdi]
	sub rdi,8
	test	r9b,1		;checks parity
	jz	_modr128p ;jumps if even
	mov r10,1
_modr128p:
	mov rax,rdi

	call _shiftr

        ;A = A/2 at this point

_modr128loop1:
    
	push rdi
	push rsi
	mov rsi,rdi
	mov rdi,r11
	call _cmp		; cmp X - A/2
	pop rsi
	pop rdi
	cmp rax,1
	je _modr128le1

	mov rax,r11
	push rdi
	mov rdi,r11
	call _shiftl
	pop rdi

	jmp _modr128loop1
_modr128le1:

	mov rax,rdi
	call _shiftl



	;------- restores A´s last bit
	push r9
	push rsi
	push rcx

        mov r9,rdi
        add r9,8
        mov rsi,[r9]
        add rsi,r10
        mov [r9],rsi

	pop rcx
	pop rsi
	pop r9
	;--------- finished restoring A
        nop
        

        nop
_modr128loop2:
	;; while (A >=  B)
	;; if A >= X
	;;     A -= X
	;; X >> 1
    
	;; while (A >=  B)
	call _cmp          ;A - B is negative?
	cmp rax,3
	je _modr128le2
	

        ;; if A >= X
	push rsi
	mov rsi,r11
	call _cmp          ;;A - X is negative?
	pop rsi
	cmp rax,3
	je _modr128l2else

	; sub      A  -= X

        

	push rsi
	push rax
	mov rsi,r11
	mov rax,rdi
	call _sub
	pop rax
	pop rsi

        

_modr128l2else:
	push rdi
	push rax
	mov rdi,r11
	mov rax,r11
	call _shiftr

	pop rax
	pop rdi
	jmp _modr128loop2

_modr128le2:
	; el dato esta en A, y ya se reemplazó para este punto
	; tengo que pasar *rdi -> *rax al inicio como backup
	

	
	;mov r9,rax
	;mov rax,rdi
	;mov rdi,r9
	pop rax
	push rbx
	push rcx
	push rsi
	mov rbx,[rax]
	mov rsi,[rdi]
	mov [rdi],rbx
	mov [rax],rsi
	add rax,8
	add rdi,8
	mov rbx,[rax]
	mov rsi,[rdi]
	mov [rdi],rbx
	mov [rax],rsi
	sub rax,8
	sub rdi,8

	pop rsi
	pop rcx
	pop rbx


	pop r10
	pop r9

	ret
_modr128_l:
	;; copy *rdi to *rax
	pop rax
	pop r10
	pop r9
	ret




;in rdx
;out rcx
_blocktosize:
	push rdx
	push rax
	push r8

	mov r8,64
	mov rax,rdx
	xor rdx,rdx
	mul r8
	mov rcx,rax

	pop r8
	pop rax
	pop rdx

;in rcx
;out rdx
_sizetoblock:
	push rcx
	push rax
	push r8

	mov r8,64
	mov rax,rcx
	xor rdx,rdx
	div r8
	mov rdx,rax

	pop r8
	pop rax
	pop rcx


;in rdi*
;out: rax    negative? | zero?
_cmpzero:
	push rdi
	push r8
	push r9

	xor rax,rax

	mov r8,[rdi]
	add r8,0
	jl  _cmpzeroneg
	jnz _cmpzeropos

	add rdi,8
	mov r8,[rdi]
	add r8,0
	jz _cmpzeroz
	jmp _cmpzeropos

_cmpzeroz:
	mov rax,1
	pop r9
	pop r8
	pop rdi
	ret
_cmpzeropos:
	mov rax,0
	pop r9
	pop r8
	pop rdi
	ret

_cmpzeroneg:
	mov rax,2
	pop r9
	pop r8
	pop rdi
	ret

;modularexp
;in rdi* x,   rsi* y,  rbx*  p, rcx = size, 
; r10* temp1, r11* temp2
;out rax*
_modularexp:
	push r8
	push r9
	push r10
	push r11
	push rdx
	
	; initialice result in 1
	; Clears temps 
	call _cleardata  ;rax
	mov r8,rax
	mov rax,r10
	call _cleardata  ;temp1
	mov rax,r11
	call _cleardata  ;temp2
	mov rax,r8
	mov r10,rcx
	call _lenrcx
	mov rdx,1
	mov r8,rax
	add r8,rcx
	mov [r8],rdx
	mov rcx,r10


	;  x = x%p
	push rsi
	push rax
	mov rsi,rbx
	mov rax,rdi
	call _modr128
	pop rax
	pop rsi

_modularexp_loop:
	push rdi
	push rax
	mov rdi,rsi
	call _cmpzero
	mov r9,rax
	pop rax
	pop rdi
	add r9,0
	jnz _modularexp_exit
	


	mov r8,rsi
	add r8,8
	mov r9,[r8]
	test r9b,1		;checks parity
	jz	_modularexp_cont ;jumps if even
	
	push rsi
	push rdi
	push r8
	mov rsi,rdi
	mov rdi,rax
	mov r8,r10
	call _mult128

	mov rsi,rbx
	call _modr128
	pop r8
	pop rdi
	pop rsi
	
_modularexp_cont:
	; y  = y>>1
	push rdi
	push rax
	mov rdi,rsi
	mov rax,rsi
	call _shiftr
	pop rax
	pop rdi

	;x = (x*x)%p
	push rsi
	push rax
	push r8
	mov r8,r10
	mov rsi,rdi
	mov rax,rdi
	call _mult128
	mov rsi,rbx
	call _modr128
	pop r8
	pop rax
	pop rsi

	jmp _modularexp_loop

_modularexp_exit:
	
	
	pop rdx
	pop r11
	pop r10
	pop r9
	pop r8
	
	ret












; NOT FUNCTIONAL
;rsi first* , rbx second*
;rcx = size
;r8 temp1*, r10 temp2*
;rax* out
_getmodule:
	push rdi
	push rsi
	push rbx
	push rdx
	push rcx
	push r9

	; clears temp 1 2, and result 
	call _cleardata
	mov r9,rax
	mov rax,r8
	call _cleardata
	mov rax,r10
	call _cleardata
	mov rax,r9

	call _sizetoblock
	mov rsi,rdi
	mov rdi,r8
	mov rcx,rax

	;call generic_size_modulus

	mov rax,rcx
	
	pop r9
	pop rcx
	pop rdx
	pop rbx
	pop rsi
	pop rdi
	ret


