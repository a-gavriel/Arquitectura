# Para compilar ambos archivos
Se puede utilizar el archivo Makefile ejecutando el comando:
```
make
```
Esto generará todos los archivos requeridos para el análisis.


## Para visualizar la información generada por Callgrind se puede utilizar
```
kcachegrind callgrind.out.XXXXXX
```
En donde XXXX se remplaza por el nombre correspondiente entre "static" o "no". 


### Archivos para el análisis del punto 4
* E1-static.asm
* E1-no.asm

### Archivos para el análisis del punto 5
* ELF-static-S.txt
* ELF-no-S.txt

### Archivos para el análisis del punto 6
* callgrind.out.static
* callgrind.out.no

