int method_with_static_variables(int x, int y , int z){
    
    static int counter;
    counter++;
    return (x+y+z+counter);
}
int main(){
    for (int i = 0;i<200000;++i){
        method_with_static_variables(i,i,i);
    }
}